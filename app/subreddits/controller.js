module.exports = function(Subreddits) {
  const getList = (req, res) => {
    Subreddits.getList().then(data => {
      res.json({data})
    }).catch(error => {
      res.json({error})
    })
  }

  const saveList = (req, res) => {
    Subreddits.saveList(req.body.list).then(data => {
      res.json({success: true})
    }).catch(error => {
      res.json({error})
    })
  }

  const getSingle = (req, res) => {
    Subreddits.getSingle(req.params.name).then(data => {
      res.json({data})
    }).catch(error => {
      res.json({error})
    })
  }

  return {
    getList,
    saveList,
    getSingle
  }
}
