const assert = require('assert')
let sinon = require('sinon')
const sinonStubPromise = require('sinon-stub-promise');
sinonStubPromise(sinon);

const httpMocks = require('node-mocks-http')

const model = require('./model')
const redis = require('../utils/redis')
const controller = require('./controller')

describe('subreddits controller', () => {
  it('is defined', () => {
    assert(controller)
  })

  it('returns json', () => {
    const testCases = [
      {
        data: ['a', 's'],
        response: { data: ['a', 's'] },
        method: 'getList'
      },
      {
        data: '',
        response: { success: true },
        method: 'saveList'
      },
      {
        data: ['a', 's'],
        response: { data: ['a', 's'] },
        method: 'getSingle'
      }
    ]

    testCases.forEach(t => {
      let req = httpMocks.createRequest()
      let res = httpMocks.createResponse()
      let Subs = model(redis.createClient())

      let jsonSpy = sinon.spy(res, 'json')
      let methodStub = sinon.stub(Subs, t.method).returnsPromise().resolves(t.data)

      controller(Subs)[t.method](req, res)

      assert(jsonSpy.called);
      assert(methodStub.called);
      assert.deepEqual(jsonSpy.args[0][0], t.response);
    })
  })
})
