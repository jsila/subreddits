const axios = require('axios')
const map = require('lodash/map')

module.exports = function(client, expireDuration = 60*60*12, key = 'subreddits') {

  const getList = () => client.smembersAsync(key)

  const saveList = subreddits => client.saddAsync(key, subreddits)

  const getSingle = subreddit => {

    return client.sismemberAsync(key, subreddit).then(isMember => {
      if (!isMember) {
        return Promise.reject("Subreddit not on the list.")
      }

      const keyS = `${key}:${subreddit}`

      return client.getAsync(keyS).then(data => {
        if (data) {
          return Promise.resolve(JSON.parse(data))
        }
        return axios.get(`https://www.reddit.com/r/${subreddit}.json`).then(data => {
          data = map(data.data.data.children, c => ({
            subreddit:    c.data.subreddit,
            domain:       c.data.domain,
            author:       c.data.author,
            thumbnail:    c.data.thumbnail,
            url:          c.data.url,
            title:        c.data.title,
            created_at:   c.data.created_at,
            num_comments: c.data.num_comments,
            ups:          c.data.ups,
            permalink:    c.data.permalink
          }))
          return client.setAsync(keyS, JSON.stringify(data))
            .then(() => client.expireAsync(keyS, expireDuration))
            .then(() => Promise.resolve(data))
       })
      })
    })
  }

  return {
    getList,
    saveList,
    getSingle
  }
}
