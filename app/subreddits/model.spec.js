
const proxyquire = require('proxyquire')
const assert = require('assert')
const sinon = require('sinon')
require('sinon-stub-promise')(sinon)

const redis = require('../utils/redis')
const axios = require('axios')
const model = proxyquire('./model', {
  'axios': axios
})

let axiosGetSpy, axiosGetStub

describe('subreddits model', () => {

  afterEach(() => {
    axiosGetSpy && axiosGetSpy.restore()
    axiosGetStub && axiosGetStub.restore()
  })

  it('exists', () => {
    assert(require('./model'))
  })

  it('returns error if subreddit not on the list', () => {
    let k = 'subr'

    let client = redis.createClient()

    let sismemberAsyncStub = sinon.stub(client, 'sismemberAsync').returnsPromise().resolves(null)
    getAsyncSpy = sinon.spy(client, 'getAsync')

    model(client).getSingle(k).catch(err => {
      assert.equal(err, "Subreddit not on the list.")
    })

    assert.equal(sismemberAsyncStub.called, 1)
    assert.ok(sismemberAsyncStub.calledWith('subreddits', 'subr'))
  })

  it('returns saved data in subreddit on list', () => {
    let k = 'subr'
    let data = '[1,2,3]'
    let client = redis.createClient()

    sinon.stub(client, 'sismemberAsync').returnsPromise().resolves(true)
    let getAsyncStub = sinon.stub(client, 'getAsync').returnsPromise().resolves(data)
    axiosGetSpy = sinon.spy(axios, 'get')

    model(client).getSingle(k).then(d => {
      assert.deepEqual(d, JSON.parse(data))
    })

    assert.equal(getAsyncStub.called, 1)
    assert.ok(getAsyncStub.calledWith(`subreddits:${k}`))
    assert.equal(axiosGetSpy.called, 0)
  })

  it('requests content if subreddit on the list and it\'s content not in database', () => {
    let k = 'subr'
    let client = redis.createClient()

    sinon.stub(client, 'sismemberAsync').returnsPromise().resolves(true)
    sinon.stub(client, 'getAsync').returnsPromise().resolves(null)

    axiosGetStub = sinon.stub(axios, 'get').returnsPromise().resolves(require('./testdata/requestSubredditResponse'))
    let setAsyncStub = sinon.stub(client, 'setAsync').returnsPromise().resolves()
    let expireAsyncStub = sinon.stub(client, 'expireAsync').returnsPromise().resolves()

    model(client).getSingle(k).then(d => {
      assert.deepEqual(d, require('./testdata/requestSubredditData'))
    })

    assert.equal(axiosGetStub.called, 1)
    assert.ok(axiosGetStub.calledWith(`https://www.reddit.com/r/${k}.json`))
    assert.equal(setAsyncStub.called, 1)
    assert.equal(expireAsyncStub.called, 1)

  })
})
