const express = require('express')

const model = require('./model')
const redis = require('../utils/redis')
const saveListSchema = require('./schemas/saveList')
const validate = require('../utils/middlewares/validate')

let router = express.Router()

const controller = require('./controller')(model(redis.createClient()))

router.get('/', controller.getList)
router.post('/', validate(saveListSchema), controller.saveList)
router.get('/:name', controller.getSingle)

module.exports = router
