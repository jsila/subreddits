const express = require('express')
const helmet = require('helmet')
const bodyParser = require('body-parser')

const subreddits = require('./subreddits')

let app = express()

app.use(bodyParser.json())
// app.use(helmet())
// app.use(helmet.contentSecurityPolicy({
//   directives: {
//     scriptSrc: ["'self'"],
//     styleSrc: ["'self'", "'fonts.googlepapis.com'"]
//   }
// }))

app.use('/subreddits', subreddits)

let server = app.listen(8080, () => {
  console.log(`Serving at http://localhost:${server.address().port}`);
})
