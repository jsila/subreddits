const validator = require('is-my-json-valid')

module.exports = schema => (req, res, next) => {
  let validate = validator(schema)
  if (validate(req.body)) {
    next()
  } else {
    res.status(400).json({ errors: validate.errors })
  }
}
