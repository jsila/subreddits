const webpack = require('webpack')
const {resolve} = require('path')

const paths = {
  public: resolve('public')
}

module.exports = {
  entry: [
//    'webpack-dev-server/client?http://localhost:8080',
    resolve('src', 'main.js')
  ],
  output: {
    filename: 'main.js',
    path: paths.public
  },
  module: {
    loaders: [{
      test: /\.js$/,
      loaders: ['babel']
    }]
  },
  devtools: 'eval-source-map',
  resolve: {
    extensions: ['', '.js']
  },
  devServer: {
    contentBase: paths.public + '/'
  },
  plugins: [
    new webpack.NoErrorsPlugin()
  ]
}
