export default class StorageService {
  constructor(key, storage) {
    this.key = key
    this.storage = storage
  }
  save(data) {
    if (typeof data != 'string') {
      data = JSON.stringify(data)
    }
    this.storage.setItem(this.key, data)
  }
  load() {
    try {
      return JSON.parse(this.storage[this.key])
    } catch (e) {
      return {}
    }
  }
  exists() {
    return (this.key in this.storage)
  }
}
