import axios from 'axios'
import Rx from '@reactivex/rxjs'
import moment from 'moment'
import includes from 'lodash/includes'
import map from 'lodash/map'
import capitalize from 'lodash/capitalize'

import StorageService from './StorageService'

const selectElement = selector => document.querySelector(selector)
HTMLElement.prototype.createObservable = function(type, key = 'observables') {
  if (!this.hasOwnProperty(key)) {
    this[key] = {};
  }
  if (!this[key][type]) {
    this[key][type] = Rx.Observable.fromEvent(this, type);
  }
  return this[key][type];
}

const renderSubreddit = ({ data }) => {
  let permalink = `//www.reddit.com${data.permalink}`
  let userPermalink = `//www.reddit.com/u/${data.author}`
  let subredditPermalink = `//www.reddit.com/r/${data.subreddit}`
  let submitTimeRelative = moment.unix(data.created_utc).fromNow()

  let img = includes(data.thumbnail, 'http') ? `<div class="card-image pull-md-left card-image" style="width: 20%;">
    <img class="img-fluid" src="${data.thumbnail}" alt="Thumbnail for ${data.title}">
  </div>` : ''

  let width = img ? 80 : 100;

  return `<div class="card clearfix">
    ${img}
    <div class="card-block pull-md-left" style="width:${width}%;">
      <h4 class="card-title"><a target="__blank" href="${data.url}">${data.title}</a> <small class="text-muted">(${data.domain})</small></h4>
      <p class="card-text">Submitted ${submitTimeRelative} by <a target="__blank" href="${userPermalink}">${data.author}</a> to <a target="__blank" href="${subredditPermalink}">/r/${data.subreddit}</a></p>
      <small class="card-text text-muted">Upvoted ${data.ups} times</small> &middot; <small class="card-text text-muted"><a target="__blank" href="${permalink}">${data.num_comments} comments</a></small>
    </div>
  </div>`
}

const renderOptions = subreddits => {
  subreddits.sort()
  return '<option value="none" selected disabled>Pick subreddit</option>' + map(subreddits, subreddit => {
    return `<option value="${subreddit}">${subreddit}</option>`
  }).join('')
}

const createURL = (change, click, type) => {
  let subreddit = change.target.selectedOptions[0].value

  if (!type) {
    return `//www.reddit.com/r/${subreddit}.json`
  }

  if (subreddit) {
    subreddit = 'r/' + subreddit
  }

  return `//www.reddit.com/${subreddit}.json?count=25&${type}=${click.target.dataset[type]}`
}

const resetUI = () => {
  loader.style.display = 'block'
  content.parentNode.classList.add('hidden')
  content.innerHTML = ''

  newerBtn.removeAttribute('disabled')
  newerBtn.dataset.before = undefined

  olderBtn.removeAttribute('disabled')
  olderBtn.dataset.after = undefined
}

const setPaginationButton = (btn, type, data) => {
  if (!data.data[type]) {
    btn.classList.add('disabled')
  } else {
    btn.classList.remove('disabled')
    btn.dataset[type] = data.data[type]
  }
}

let subredditsStorage = new StorageService('subreddits', window.localStorage)

let content = selectElement('#subreddit-content')
let loader = selectElement('#subreddit-loader')

let select = selectElement('select')
let selectChange = select.createObservable('change')

let form = selectElement('form')
let formSubmit = form.createObservable('submit')

let newerBtn = selectElement('#newer')
let newerBtnClick = newerBtn.createObservable('click')

let olderBtn = selectElement('#older')
let olderBtnClick = olderBtn.createObservable('click')

let uploadBtn = selectElement('#uploadBtn')
let uploadBtnClick = uploadBtn.createObservable('click')

let upload = selectElement('#upload')
let uploadChange = upload.createObservable('change')

const request = formSubmit
  .map(e => e.preventDefault())
  .withLatestFrom(selectChange, (submit, change) => createURL(change))
  .merge(olderBtnClick.withLatestFrom(selectChange, (click, change) =>  createURL(change, click, 'after')))
  .merge(newerBtnClick.withLatestFrom(selectChange, (click, change) =>  createURL(change, click, 'before')))

request.subscribe(() => {
  content.parentNode.classList.remove('hidden')
})

const response = request
  .flatMap(url => Rx.Observable.from(axios.get(url)))

response.subscribe(data => {
  window.scrollTo(0,0)

  loader.style.display = 'none'
  content.innerHTML = data.data.data.children.map(renderSubreddit).join('')

  setPaginationButton(olderBtn, 'after', data.data)
  setPaginationButton(newerBtn, 'before', data.data)
})

uploadBtnClick.map(e => e.preventDefault())
  .subscribe(() => upload.click())

uploadChange.subscribe(e => {
    let reader = new FileReader()
    reader.readAsText(e.target.files[0], "utf-8")
    reader.onload = evt => {
      subredditsStorage.save(evt.target.result)
      select.innerHTML = renderOptions(JSON.parse(evt.target.result))
      resetUI()
    }
  })

let localSubreddits = subredditsStorage.exists() ? subredditsStorage.load() : ["", "aww", "golang", "askreddit"]

select.innerHTML = renderOptions(localSubreddits)
